/*
 * Copyright 2015 Red Hat Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

var url = require('url');

var simple_id_generator = {
    counter : 1,
    next : function() {
        return this.counter++;
    }
};

class Client {
    constructor(connection, address) {
        //var u = url.parse(address);
        //TODO: handle scheme and user/password if present
        this.connection = connection;
        //this.connection = container.connect({'host':u.hostname, 'port':u.port});
        this.connection.on('message', this._response.bind(this));
        this.connection.on('receiver_open', this._ready.bind(this));
        //this.sender = this.connection.attach_sender(u.path.substr(1));
        this.sender = this.connection.attach_sender(address);
        this.receiver = this.connection.attach_receiver({source:{dynamic:true}});
        this.id_generator = simple_id_generator;
        /* requests yet to be made (waiting for receiver to open) */
        this.pending = [];
        /* requests sent, for which responses have not yet been received */
        this.outstanding = {};
    }

    _request(id, name, args, callback) {
        var request = {};
        request.subject = name;
        request.body = args;
        request.correlation_id = id;
        request.reply_to = this.receiver.remote.attach.source.address;
        this.outstanding[id] = callback;
        this.sender.send(request);
    }

    _response(context) {
        var id = context.message.correlation_id;
        var callback = this.outstanding[id];
        if (callback) {
            callback(null, context.message);
        } else {
            console.error('no request pending for ' + id + ', ignoring response');
        }
    }

    _ready() {
        this._process_pending();
    }

    _process_pending() {
        for (var i = 0; i < this.pending.length; i++) {
            var r = this.pending[i];
            this._request(r.id, r.name, r.args, r.callback);
        }
        this.pending = [];
    }

    call(name, args, callback) {
        var id = this.id_generator.next();
        if (this.receiver.is_open() && this.pending.length === 0) {
            this._request(id, name, args, callback);
        } else {
            //need to wait for reply-to address
            this.pending.push({'name':name, 'args':args, 'callback':callback, 'id':id});
        }
    }

    close() {
        this.receiver.close();
        this.sender.close();
        this.connection.close();
    }

    define(name) {
        this[name] = function (args, callback) { this.call(name, args, callback); };
    }
}


class Cache {
    constructor(ttl, purged) {
        this.ttl = ttl;
        this.purged = purged;
        this.entries = {};
        this.timeout = undefined;
    }

    clear() {
        if (this.timeout) clearTimeout(this.timeout);
        this.entries = {};
    }

    put(key, value) {
        this.entries[key] = {'value':value, 'last_accessed': Date.now()};
        if (!this.timeout) this.timeout = setTimeout(this.purge.bind(this), this.ttl);
    }

    get(key) {
        var entry = this.entries[key];
        if (entry) {
            entry.last_accessed = Date.now();
            return entry.value;
        } else {
            return undefined;
        }
    }

    purge() {
        //TODO: this could be optimised if the map is large
        var now = Date.now();
        var expired = [];
        var live = 0;
        for (var k in this.entries) {
            if (now - this.entries[k].last_accessed >= this.ttl) {
                expired.push(k);
            } else {
                live++;
            }
        }
        for (var i = 0; i < expired.length; i++) {
            var entry = this.entries[expired[i]];
            delete this.entries[expired[i]];
            this.purged(entry.value);
        }
        if (live && !this.timeout) {
            this.timeout = setTimeout(this.purge.bind(this), this.ttl);
        }
    }
}


class LinkCache {
    constructor(factory, ttl) {
        this.factory = factory;
        this.cache = new Cache(ttl, function(link) { link.close(); });
    }

    clear() {
        this.cache.clear();
    }

    get(address) {
        var link = this.cache.get(address);
        if (link === undefined) {
            link = this.factory(address);
            this.cache.put(address, link);
        }
        return link;
    }
}

function match(desired, offered) {
    if (offered) {
        if (Array.isArray(offered)) {
            return offered.indexOf(desired) > -1;
        } else {
            return desired === offered;
        }
    } else {
        return false;
    }
}


class Server {
    constructor(container, address, options) {
        this.options = options || {};
        var u = url.parse(address);
        //TODO: handle scheme and user/password if present
        this.connection = container.connect({'host':u.hostname, 'port':u.port});
        this.connection.on('connection_open', this._connection_open.bind(this));
        this.connection.on('message', this._request.bind(this));
        this.receiver = this.connection.attach_receiver(u.path.substr(1));
        this.callbacks = {};
        this._send = undefined;
        this._clear = undefined;
    }

    _connection_open() {
        if (match('ANONYMOUS-RELAY', this.connection.remote.open.offered_capabilities)) {
            var relay = this.connection.attach_sender({target:{}});
            this._send = function (msg) { relay.send(msg); };
        } else {
            var cache = new LinkCache(this.connection.attach_sender.bind(this.connection), this.options.cache_ttl || 60000);
            this._send = function (msg) { var s = cache.get(msg.to); if (s) s.send(msg); };
            this._clear = function () { cache.clear(); };
        }
    }

    _respond(response) {
        var server = this;
        return function (result, error) {
            if (error) {
                response.subject = error.name || 'error';
                response.body = error.description || error;
            } else {
                response.subject = 'ok';
                response.body = result;
            }
            server._send(response);
        };
    }

    _request(context) {
        var request = context.message;
        var response = {};
        response.to = request.reply_to;
        response.correlation_id = request.correlation_id;
        var callback = this.callbacks[request.subject];
        if (callback) {
            callback(request.body, this._respond(response));
        } else {
            response.subject = 'bad-method';
            response.body = 'Unrecognised method ' + request.subject;
            this._send(response);
        }
    }

    bind_sync(f, name) {
        this.callbacks[name || f.name] = function (args, callback) { var result = f(args); callback(result); };
    }

    bind(f, name) {
        this.callbacks[name || f.name] = f;
    }

    close() {
        if (this._clear) this._clear();
        this.receiver.close();
        this.connection.close();
    }
}

module.exports = {
    server : function(container, address, options) { return new Server(container, address, options); },
    client : function(connection, address) { return new Client(connection, address); }
};
